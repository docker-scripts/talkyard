# talkyard

## Requirements

In the host system we need to fix these kernel settings, otherwise the
ElasticSearch container will not start:

```bash
cat <<EOF > /etc/sysctl.conf
net.core.somaxconn = 8192
vm.max_map_count = 262144
EOF

sysctl -p /etc/sysctl.conf
```

For other optimizations that may be done on the host, look at
[scripts/prepare-os.sh](https://github.com/debiki/talkyard-prod-one/blob/main/scripts/prepare-os.sh).

## Installation

  - First install `ds` and `revproxy`:
    + https://gitlab.com/docker-scripts/ds#installation
    + https://gitlab.com/docker-scripts/revproxy#installation

  - Install `yq`: https://github.com/mikefarah/yq#wget

  - Then get the scripts: `ds pull talkyard`

  - Create a directory for the container: `ds init talkyard @talkyard.example.org`

  - Personalize the settings:
    `cd /var/ds/talkyard.example.org/ ; vim settings.sh`

  - Build and start the containers: `ds make`

  - Open https://talkyard.example.org and continue the setup

## Update

- Find the latest version tag at:
  https://github.com/debiki/talkyard-versions/blob/master/version-tags.log

- Edit `settings.sh` and set the latest tag.

- Update with: `ds update`
