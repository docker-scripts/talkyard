APP=talkyard
DOMAIN='talkyard.example.org'
VERSION='v0.2024.004-678d1190f'
# see: https://github.com/debiki/talkyard-versions/blob/master/version-tags.log

ADMIN_EMAIL='admin@example.org'

### Uncomment to use a network that is different from the default one.
#INTERNAL_NET="172.26.0"    # only 3 digits

### SMTP server used for sending notification emails.
### You can build an SMTP server as described here:
### https://gitlab.com/docker-scripts/postfix
# SMTP_SERVER=smtp.example.org
# SMTP_PORT=587
# SMTP_USER=
# SMTP_PASS=
# SMTP_FROM=info@example.org
# SMTP_TLS=true
