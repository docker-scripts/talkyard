cmd_set-postgres-passwd_help() {
    cat <<_EOF
    set-postgres-passwd <new-password>
        Set a new password to postgres.

_EOF
}

cmd_set-postgres-passwd() {
    local passwd=$1
    [[ -z $passwd ]] && fail "Usage:\n$(cmd_set-postgres-passwd_help)"    

    # set the password on .env
    sed -i .env \
        -e "/^POSTGRES_PASSWORD/ c POSTGRES_PASSWORD=$passwd"

    # set the password on postgres
    docker compose exec rdb psql -c "alter user talkyard password '$passwd'" talkyard talkyard
    # see: https://www.talkyard.io/-147/database-connection-error-and-a-question
    
    # restart
    ds restart
}
