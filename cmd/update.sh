cmd_update_help() {
    cat <<_EOF
    update
        Update to the latest version of talkyard.

_EOF
}

cmd_update() {
    # get the latest version
    local VERSION_TAG=$(curl -s https://raw.githubusercontent.com/debiki/talkyard-versions/master/version-tags.log | tail -1)
    echo "Current version: $VERSION"
    echo "Latest version:  $VERSION_TAG"
    if [[ $VERSION_TAG == $VERSION ]]; then
        echo "There is no new version. No update is needed."
        exit
    fi
    read -p "Press Enter to continue, or Ctrl+C to cancel..."

    # make a backup first, just in case
    set -x
    ds backup

    # download the new images
    docker compose pull

    # stop and delete the currents containers
    docker compose stop search
    docker compose stop app
    docker compose down

    # build and start the new containers
    sed -i settings.sh \
        -e "/^VERSION/ c VERSION=$VERSION_TAG"
    sed -i .env \
        -e "/^VERSION_TAG/ c VERSION_TAG=$VERSION_TAG"
    docker compose \
        -f docker-compose.yml \
        -f docker-compose.mem.yml \
        -f docker-compose.ds.yml \
        up -d
}
