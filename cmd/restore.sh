# Based on:
# https://github.com/debiki/talkyard-prod-one/blob/main/docs/how-restore-backup.md

cmd_restore_help() {
    cat <<_EOF
    restore <backup-file.tgz> [-c]
        Restore from the given backup file.
        If the '-c' option is given, restore the config files as well.
        By default only the postgresql data and uploads are restored.

_EOF
}

cmd_restore() {
    local file=$1
    [[ ! -f $file ]] && fail "Usage:\n$(cmd_restore_help)"    
    local backup=${file%%.tgz}
    backup=$(basename $backup)
    local restore_config=$2

    # extract the backup archive
    tar --extract --gunzip --preserve-permissions --file=$file

    # stop all the containers
    ds stop

    # restore uploads
    rsync -a $backup/uploads/ data/uploads/

    # restore config files
    if [[ $restore_config == '-c' ]]; then
        cp $backup/settings.sh .
        cp $backup/.env .
        cp $backup/docker-compose.* .
        cp $backup/play-framework.conf conf/

        ds make
        ds stop
    fi

    # restore the content of the database
    sed -i $backup/postgres.sql \
        -e 's/^DROP ROLE/-- DROP ROLE/' \
        -e 's/^CREATE ROLE/-- CREATE ROLE/' \
        -e 's/^ALTER ROLE/-- ALTER ROLE/'
    docker compose up -d rdb
    sleep 3
    cat $backup/postgres.sql \
        | docker exec -i $(docker compose ps -q rdb) psql postgres postgres

    # clean up
    rm -rf $backup

    # start
    ds start
}
