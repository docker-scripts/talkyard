cmd_backup_help() {
    cat <<_EOF
    backup
        Make a backup.

_EOF
}

cmd_backup() {
    # create the backup dir
    local backup="backup-$(date +%Y%m%d)"
    rm -rf $backup
    rm -f $backup.tgz
    mkdir $backup

    # dump the content of the database
    docker compose exec -T rdb pg_dumpall --username=postgres --clean --if-exists > $backup/postgres.sql

    cp settings.sh .env docker-compose.* conf/play-framework.conf $backup/
    cp -a data/uploads/ $backup/
    cp talkyard-prod-one/docs/how-restore-backup.md $backup/ 

    # make the backup archive
    tar --create --gzip --preserve-permissions --file=$backup.tgz $backup/
    rm -rf $backup/
    mv $backup.tgz backups/
}
