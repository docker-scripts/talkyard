rename_function cmd_revproxy global_cmd_revproxy
cmd_revproxy() {
    global_cmd_revproxy "$@"
    [[ $1 == 'add' ]] &&  _customize_revproxy_config
}

_customize_revproxy_config() {
    # replace https by http
    local type=$(ds revproxy type)
    local config_file=$(ds revproxy path)
    local match_proxy_directive='proxy_pass'
    [[ $type == 'apache2' ]] && match_proxy_directive='ProxyPass'
    sed -i $config_file \
        -e "/$match_proxy_directive/ s/https/http/"

    # reload the new configuration
    ds @revproxy reload
}
