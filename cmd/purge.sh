cmd_purge_help() {
    cat <<_EOF
    purge
        Remove and cleanup all data and config files.

_EOF
}

cmd_purge() {
    ds remove
    rm -rf data/ log/ conf/ *.yml .env
}
