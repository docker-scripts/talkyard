cmd_copy-ssl-cert_help() {
    cat <<_EOF
    copy-ssl-cert
        Copy the SSL cert from 'revproxy/letsencrypt/' to 'talkyard/data/sslcert/'

_EOF
}

cmd_copy-ssl-cert() {
    local ssl_cert_dir=$CONTAINERS/revproxy/letsencrypt/live/$DOMAIN
    mkdir -p data/sslcert/
    cp -fL $ssl_cert_dir/fullchain.pem data/sslcert/
    cp -fL $ssl_cert_dir/privkey.pem   data/sslcert/
}
