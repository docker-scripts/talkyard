#!/bin/bash

cmd_start() {
    docker compose start
}

cmd_stop() {
    docker compose stop
}

cmd_restart() {
    docker compose restart
}

cmd_shell() {
    local container=${1:-web}
    docker compose exec -u root $container bash
}

cmd_remove() {
    docker compose down
    ds revproxy rm

    rm -f /etc/cron.d/"$(echo $(basename $(pwd)) | tr . -)"-copy-ssl-cert
}

cmd_make() {
    hash yq 2>/dev/null \
        || fail "Dependency 'yq' is missing.\nSee: https://github.com/mikefarah/yq#wget"

    _host_config

    # get docker-compose.yml
    cp talkyard-prod-one/docker-compose.yml .
    cp talkyard-prod-one/mem/2g.yml docker-compose.mem.yml

    # remove the attribute 'version', which is obsolete
    sed -i docker-compose.yml \
        -e '/^version:/ d'
    sed -i docker-compose.mem.yml \
        -e '/^version:/ d'

    # make relative to the current directory all the mounted volumes
    sed -i docker-compose.yml \
        -e 's#- /var/log/#- ./log/#' \
        -e 's#- /opt/talkyard-backups/#- ./backups/#'

    # delete the ports of the web service because it will be served by revproxy
    yq eval --inplace 'del(.services.web.ports)' docker-compose.yml

    # delete .services.web.volumes
    yq eval --inplace 'del(.services.web.volumes)' docker-compose.yml

    # delete .services.cache.sysctls
    yq eval --inplace 'del(.services.cache.sysctls)' docker-compose.yml

    # delete .services.search.ulimits
    yq eval --inplace 'del(.services.search.ulimits)' docker-compose.yml

    cat <<EOF > docker-compose.ds.yml
# Add the docker-scripts network as an external network
# and connect the web service to this network.
# Add also a network-alias to the web container on the docker-scripts
# network, so that revproxy can access it by the domain name.

networks:
  docker-scripts:
    name: $NETWORK
    external: true

services:
  web:
    volumes:
      - ./conf/sites-enabled-manual/:/etc/nginx/sites-enabled-manual/:ro
      - ./data/sslcert/:/etc/certbot/live/$DOMAIN/:ro
      - ./data/uploads/:/opt/talkyard/uploads/:ro
      - ./log/nginx/:/var/log/nginx/
    networks:
      docker-scripts:
        aliases:
          - $DOMAIN
EOF

    # get and customize .env
    if [[ ! -f .env ]]; then
        cp talkyard-prod-one/.env .
        local postgress_password=$(tr -cd '[:alnum:]' < /dev/urandom | head -c30)
        sed -i .env \
            -e "/^COMPOSE_PROJECT_NAME/ c COMPOSE_PROJECT_NAME=${CONTAINER//./-}" \
            -e "/^POSTGRES_PASSWORD/ c POSTGRES_PASSWORD=$postgress_password" \
            -e "/^VERSION_TAG/ c VERSION_TAG=$VERSION" \
            -e "s/172\.26\.0/${INTERNAL_NET:-172.26.0}/g"
    fi

    # get and customize conf/ files
    if [[ ! -d conf/ ]]; then
        cp -a talkyard-prod-one/conf/ .
        local secret_key=$(tr -cd '[:alnum:]' < /dev/urandom | head -c80)
        sed -i conf/play-framework.conf \
            -e "/talkyard.becomeOwnerEmailAddress/ c talkyard.becomeOwnerEmailAddress=\"$ADMIN_EMAIL\"" \
            -e "/talkyard.hostname/ c talkyard.hostname=\"$DOMAIN\"" \
            -e "/talkyard.secure/ c talkyard.secure=true" \
            -e "/play.http.secret.key/ c play.http.secret.key=\"$secret_key\""
        sed -i conf/play-framework.conf \
            -e "/talkyard.smtp.host/ c talkyard.smtp.host=\"$SMTP_SERVER\"" \
            -e "/talkyard.smtp.port/ c talkyard.smtp.port=${SMTP_PORT:-25}" \
            -e "/talkyard.smtp.requireStartTls/ c talkyard.smtp.requireStartTls=${SMTP_TLS:-true}" \
            -e "/talkyard.smtp.user/ c talkyard.smtp.user=\"$SMTP_USER\"" \
            -e "/talkyard.smtp.password/ c talkyard.smtp.password=\"$SMTP_PASS\"" \
            -e "/talkyard.smtp.fromAddress/ c talkyard.smtp.fromAddress=\"$SMTP_FROM\""
    fi

    # build and start the containers
    docker compose \
        -f docker-compose.yml \
        -f docker-compose.mem.yml \
        -f docker-compose.ds.yml \
        up -d

    ds revproxy add
    ds revproxy ssl-cert
    _copy_ssl_cert
}

_host_config() {
    apt install --yes locales tree ncdu
    locale-gen en_US.UTF-8
    export LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8
}

_copy_ssl_cert() {
    # copy the ssl cert from revproxy to talkyard
    ds copy-ssl-cert

    # create a cron job that copies the ssl cert from revproxy once a week
    local dir=$(basename $(pwd))
    mkdir -p /etc/cron.d
    cat <<EOF > /etc/cron.d/"$(echo $dir | tr . -)"-copy-ssl-cert
# copy the ssl cert @$dir each week
0 0 * * 0  root  bash -l -c "ds @$dir copy-ssl-cert &> /dev/null"
EOF

}
