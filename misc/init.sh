# run custom init steps

# install yq
if ! hash yq; then
    wget https://github.com/mikefarah/yq/releases/download/v4.30.8/yq_linux_amd64 \
         -O /usr/bin/yq
    chmod +x /usr/bin/yq
fi

# get the code of talkyard-prod-one
git clone https://github.com/debiki/talkyard-prod-one.git
